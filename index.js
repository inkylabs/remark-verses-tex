import { visit } from 'unist-util-visit'

export default (opts) => {
  return (root, f) => {
    visit(root, 'verseref', (node, index, parent) => {
      node.type = 'tex'
      node.value = node.value
        .replace(/\. /g, '.\\ ')
        .replace(/\u00A0/g, '~') // nbsp
      // This is a nasty hack to make sure synctex identifies this line with
      // the last page it appears on.
      node.value += '\\makebox(0,0){}'
    })
  }
}
