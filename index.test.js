/* eslint-env jest */
import mod from '.'

describe('simple', () => {
  const f = mod();
  [
    ['1\u00A0Cor. 2:2; Heb. 6:1', '1~Cor.\\ 2:2; Heb.\\ 6:1']
  ]
    .forEach(([i, e]) => {
      it(i, () => {
        const node = {
          type: 'verseref',
          value: i
        }
        f(node)
        expect(node.type).toEqual('tex')
        expect(node.value).toEqual(e + '\\makebox(0,0){}')
      })
    })
})
